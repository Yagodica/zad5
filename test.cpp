#include <conio.h>
#include <windows.h>
#include <vector>
using namespace std;


class shape {
public:
	virtual void show(HDC hdc) {} // виртуальный метод, позволяющий использовать метод show конкретного класса
}; // базовый класс для фигур

// класс точка
class point : shape {
public:
	int x, y;
	COLORREF color;
	point(int x, int y, COLORREF c = RGB(255, 255, 255)) : x(x), y(y), color(c) {}
	void show(HDC hdc)
	{
		SetPixel(hdc, x, y, color);
	}
};

class line : shape {
	point first, second;
	COLORREF color;

public:
	line(int x1, int y1, int x2, int y2, COLORREF c = RGB(255, 255, 255)) : first(x1, y1), second(x2, y2), color(c) {}

	void show(HDC hdc) {
		SelectObject(hdc, GetStockObject(DC_PEN)); // выбор стандартного пера
		SetDCPenColor(hdc, color); // установка цвета контура фигуры
		POINT points[2] = { {first.x, first.y}, {second.x, second.y} };
		Polyline(hdc, points, 2);
	}
};

class polygon : shape {
	vector <point> points;
	COLORREF color;
	COLORREF background_color;
	bool filled;

public:
	polygon(vector <point> pts, COLORREF c, bool filled = false, COLORREF bc = 0) : points(pts), color(c), background_color(bc), filled(filled) {}

	void show(HDC hdc) {
		SelectObject(hdc, GetStockObject(DC_PEN)); // выбор стандартного пера
		SetDCPenColor(hdc, color); // установка цвета контура фигуры
		SetDCBrushColor(hdc, background_color); // установка цвета закраски
		vector <POINT> winPoints;
		for (const auto& pt : points) {
			winPoints.push_back({ pt.x, pt.y });
		}
		if (filled)
		{
			SelectObject(hdc, GetStockObject(DC_BRUSH)); // выбор стандартной кисти
		}
		else
		{
			SelectObject(hdc, GetStockObject(NULL_BRUSH)); // отключение закраски
		}
		Polygon(hdc, winPoints.data(), winPoints.size());
	}
};

class ellipse : shape {
	int left, top, right, bottom;
	COLORREF color;
	COLORREF background_color;
	bool filled;

public:
	ellipse(int left, int top, int right, int bottom, COLORREF c, bool filled = false, COLORREF bc = 0) : left(left), top(top), right(right), bottom(bottom), color(c), background_color(bc), filled(filled) {}

	void show(HDC hdc) {
		SelectObject(hdc, GetStockObject(DC_PEN)); // выбор стандартного пера
		SetDCPenColor(hdc, color); // установка цвета контура фигуры
		SetDCBrushColor(hdc, background_color); // установка цвета закраски
		if (filled)
		{
			SelectObject(hdc, GetStockObject(DC_BRUSH)); // выбор стандартной кисти
		}
		else
		{
			SelectObject(hdc, GetStockObject(NULL_BRUSH)); // отключение закраски
		}
		Ellipse(hdc, left, top, right, bottom);
	}
};

// класс прямоугольник, использующий класс точка
class rectangle : shape {
	point top_left;
	point bottom_right;
	COLORREF color;
	COLORREF background_color;
	bool filled;
public:
	rectangle(int x1, int y1, int x2, int y2, COLORREF c, bool filled = false, COLORREF bc = 0) : top_left(x1, y1), bottom_right(x2, y2), color(c), background_color(bc), filled(filled) {}
	void show(HDC hdc) {
		SelectObject(hdc, GetStockObject(DC_PEN)); // выбор стандартного пера
		SetDCPenColor(hdc, color); // установка цвета контура фигуры
		SetDCBrushColor(hdc, background_color); // установка цвета закраски
		if (filled)
		{
			SelectObject(hdc, GetStockObject(DC_BRUSH)); // выбор стандартной кисти
		}
		else
		{
			SelectObject(hdc, GetStockObject(NULL_BRUSH)); // отключение закраски
		}
		Rectangle(hdc, top_left.x, top_left.y, bottom_right.x, bottom_right.y);
	}
};

class canvas
{
	vector<shape*> figures; // вектор для хранения фигур
	HWND hWnd;
	HDC hdc;
public:
	canvas()
	{
		hWnd = GetConsoleWindow(); // ссылка на окно
		hdc = GetDC(hWnd);		// ссылка на контекст устройства графического вывода
	}
	~canvas()
	{
		ReleaseDC(hWnd, hdc);		// освобождение контекста устройства вывода
	}
	void add(shape* s)
	{
		figures.push_back(s);
	}
	void del(int i)
	{
		figures.erase(figures.begin() + i);
	}
	void clear() //очистка холста
	{
		figures.clear();
	}
	void show() { // отображение графических примитивов вызовом метода show()
		for (size_t i = 0; i < figures.size(); i++)
			figures[i]->show(hdc);
	}
};

int main()
{
	canvas c; // создаем холст

	c.add((shape*)new rectangle(10, 10, 100, 100, RGB(255, 0, 0), true, RGB(255, 255, 0))); // добавление прямоугольника
	c.add((shape*)new rectangle(150, 100, 200, 250, RGB(0, 0, 255), true, RGB(0, 255, 0))); // добавление прямоугольника
	c.add((shape*)new rectangle(10, 100, 200, 150, RGB(255, 100, 0))); // добавление не закрашенного прямоугольника
	c.add((shape*)new point(250, 50)); // добавление точки
	c.add((shape*)new point(200, 50, RGB(255, 0, 255))); // добавление точки
	c.add((shape*)new line(230, 60, 290, 20));
	c.add((shape*)new ellipse(100, 150, 400, 250, RGB(255, 100, 50))); // добавление не закрашенного элипса
	c.add((shape*)new ellipse(300, 250, 200, 450, RGB(255, 100, 0), true, RGB(255, 100, 0))); // добавление элипса	

	c.add((shape*)new polygon(
		{ { 500, 150 }, { 300, 600 }, { 400, 530 }, { 250, 180 } }, RGB(100, 100, 50))
	); // добавление не закрашенного элипса

	c.add((shape*)new polygon(
		{ { 1000, 500 }, { 300, 400 }, { 600, 700 }, { 250, 110 } }, RGB(0, 255, 0), true, RGB(100, 100, 50))
	); // добавление элипса

	c.show(); // вывод графических примитивов на холст

	_getch();


	return 0;
}
